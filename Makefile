# To build modules outside of the kernel tree, we run "make"
# in the kernel source tree; the Makefile there then includes this
# Makefile once again.

# This conditional selects whether we are being included from the
# kernel Makefile or not.
ifeq ($(KERNELRELEASE),)

	# Assume the source tree is where the running kernel was built
	# You should set KERNELDIR in the environment if it's elsewhere
	KERNELDIR ?= /lib/modules/$(shell uname -r)/build

	# The current directory is passed to sub-makes as argument
	PWD := $(shell pwd)

modules:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules

modules_install:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules_install

clean:
	rm -rf *.o *~ core .depend .*.cmd *.ko *.mod.c .tmp_versions

program:
	sudo insmod kernelModule.ko size=5
	sudo mknod /dev/ProducerConsumer c 250 0
	sudo chmod 777 /dev/ProducerConsumer
	gcc driver.c
	./a.out

remove:
	sudo rmmod kernelModule.ko
	sudo rm /dev/ProducerConsumer
	sudo rm Producer.txt
	sudo rm Consumer.txt
.PHONY: modules modules_install clean

else
	# called from kernel build system: just declare what our modules are
	obj-m := kernelModule.o
endif

