#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <signal.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#define DEVICE "/dev/ProducerConsumer"
#define stringLength 5
pthread_mutex_t Lock;
FILE *fp;
FILE *fc;
char* stringGenerator(int length) 
{ //This function is used to generate random strings of given length
	char *string = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	int slength = strlen(string)-1;
	char *randomString = NULL;
	int n;
	static int i = 0;
	srand((time(NULL) ^ (getpid() << 16)) + (i++));
	if(length < 1)
		length = 1;

	randomString = malloc(sizeof(char) * (length +1));
	if(randomString) 
	{
		int key = 0;
		for(n=0;n<length;n++)
		{
			key = rand() % slength;
			randomString[n] = string[++key];
		}
		randomString[length]='\0';
	return randomString;
	}
	else {
		printf("Wrong allocation");
		exit(1);
	}
}

void producer(int pname) { //This function is used by producer process. 
	int value=0,fd,ret;	
	char *string;
	fd = open(DEVICE, O_WRONLY); //Open Module.
	if(fd == -1) {
		printf("Module Can't be opened");
		exit(1);
	}	
	while(value++ < 9){
		string = stringGenerator(stringLength); //Generate a string of size 5
		fp = fopen("Producer.txt", "a");
		fprintf(fp, "String produced is: %s by the producer p:%d\n", string, pname);//Write the string produced to file.
		fclose(fp);
		//printf("I'm a producer process:%s and process name is:%d\n", string, pname);
		ret = write(fd, string,(stringLength)); //Write the string into buffer/ queue.
		if(ret != 0) { //If string is not copied correctly, then show error and exit.
			printf("Error in Writing String\n");
			exit(1);
		}
	}
	printf("%d. PRODUCER OUT\n",pname);
}

void consumer(int cname) { //This function is for consumer processes.
	int value=0,fd,ret;
	fd = open(DEVICE, O_RDONLY);
	if(fd == -1) { //checks if module is opened correctly or not.
		printf("Module can't be opened");
	}else {
		while (value++ < 9) {
			char *rstring;
			rstring = (char *)malloc(sizeof(char) * 5);
			ret = read(fd, rstring, 5);
			if(ret != 0) { //If there is an error in reading string from queue.
				printf("Error in Reading the String\n");
			}
			rstring[5] = '\0';
			fc = fopen("Consumer.txt", "a");
			fprintf(fc, "String consumed is: %s by the consumer c:%d\n", rstring, cname);//write the consumed string to file.
			fclose(fc);
			//printf("I'm a consumer process:%s\n", rstring);
		}
	}
	printf("%d. CONSUMER OUT\n",cname);
}
void main(int argv, char *argc[]) {
	pid_t pid;
	int i, flag=1, pname=0, cname=0; //flag 1 represents producer, flag 0 reprsents consumer
	for(i=0;i<10;i++) {		
		if(flag ==1) { //Fork a new producer process
			pname++;
			pid = fork();
			if(pid != 0) {//Checks for parent or newly forked producer process
				flag = 0;
				continue;
			}
			else
				break; 
		}//End of if
		else {// Fork a new consumer process
			cname++;
			pid = fork();
			if(pid != 0) { //Check for parent or newly forked consumer process
				flag = 1;
				continue;
			}
			else
				break;
		}//END of else
	}//End of for
	if(pid == 0) {
		if(flag ==1) {//Then this process is producer
			producer(pname);
		}
		else //Then this process is consumer
			consumer(cname);
	}
	
}
