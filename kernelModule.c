#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/semaphore.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#include <asm/uaccess.h>

struct semaphore empty;
struct semaphore full;
static DEFINE_MUTEX(lock);
int ret;
static int size;

struct dataS {
	char **data; //We use this char pointer array as queue.
	int front, rear; //Front is used to read from the data queue and rear is used to write to the data queue.
}virtual_device;

int device_open(struct inode *inode, struct file *filp) {
	printk(KERN_INFO "Openened device");
	return 0;
}

ssize_t device_read(struct file* flip, char* bufStoreData, size_t bufCount, loff_t *curOffset) {
	if(down_interruptible (&full) == 0) { //Down the full since data is read
		if(mutex_lock_interruptible(&lock) == 0) {//Got the lock
			if(virtual_device.front > size-1)
				virtual_device.front = 0;
			ret = copy_to_user(bufStoreData, virtual_device.data[virtual_device.front], bufCount);	//Copy the data to user space.
			virtual_device.front++; //Increment the pointer
			mutex_unlock(&lock); //Unlock the mutex
			up(&empty);//increment up since data is consumed.
		}
	}
	printk(KERN_ALERT "Readding from device:%s %zd\n", bufStoreData, strlen(bufStoreData));
	return ret;
}

ssize_t device_write(struct file* flip, char* bufSourceData, size_t bufCount, loff_t *curOffset) {
	//Kmalloc amount of memory needed and copy data.
	virtual_device.data[virtual_device.rear] = (char *)kmalloc(sizeof(char) * (bufCount+1), GFP_KERNEL);
	if(down_interruptible(&empty) == 0) { //Down the empty
		if(mutex_lock_interruptible(&lock) == 0) { //Acquire the Lock
			if(virtual_device.rear > size-1 )
				virtual_device.rear = 0;
			ret = copy_from_user(virtual_device.data[virtual_device.rear], bufSourceData, bufCount);//Copy data from user space.
			virtual_device.data[virtual_device.rear][bufCount] = '\0';
			virtual_device.rear++;	//Increment the pointer.
			mutex_unlock(&lock);	//Unlock the lock
			up(&full);	//Increment full, since string is put into queue.
		}
	}
	printk(KERN_ALERT "Writing to device:%s\n", virtual_device.data[virtual_device.rear-1]);
	printk(KERN_INFO "Length of give string is:%zd", bufCount);
        return ret;
}

int device_close(struct inode *inode, struct file *flip) {
	printk(KERN_INFO "Closing device");
	return 0;
}

static struct file_operations fops = {
	.owner = THIS_MODULE,
	.open = device_open,
	.release = device_close,
	.write = device_write,
	.read = device_read
};

struct cdev *mcdev;
int major_number;
dev_t dev_num;

#define DEVICE_NAME	"ProducerConsumer"

static int device_entry(void){
	ret = alloc_chrdev_region(&dev_num,0,1,DEVICE_NAME);
	if(ret < 0) {
		printk(KERN_ALERT "FAILED TO ALLOCATE MAJOR NUMBER");
		return ret;
	}

	major_number= MAJOR(dev_num);
	printk(KERN_INFO "ProducerConsumer: major number is:%d",major_number);
	printk(KERN_INFO "\tuse \"mkmod /dev/%s c %d 0\" for device file", DEVICE_NAME, major_number);

	mcdev = cdev_alloc();
	mcdev->ops = &fops;
	mcdev->owner = THIS_MODULE;

	ret = cdev_add(mcdev, dev_num, 1);
	if(ret < 0) {
		printk(KERN_ALERT "FAILED TO ADD TO KERNEL");
		return ret;
	}

	sema_init(&empty, size);
	sema_init(&full, 0);
	mutex_init(&lock);

	//Kmalloc the buffer
	virtual_device.data = kmalloc(sizeof(char *) * (size), GFP_KERNEL);
	return 0;
}

static void __exit device_exit(void) {

	cdev_del(mcdev);
	unregister_chrdev_region(dev_num, 1);
	printk(KERN_ALERT "Unloaded module");
}

module_param(size, int, S_IRUGO);
module_init(device_entry);
module_exit(device_exit);
